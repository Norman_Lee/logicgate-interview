package com.logicgate.farm.repository;

import com.logicgate.farm.domain.Animal;

import com.logicgate.farm.domain.Barn;
import com.logicgate.farm.domain.Color;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {

  // additional methods can be defined here
  List<Animal> findByFavoriteColorOrderByBarnAsc(Color color);
  List<Animal> findByBarn(Barn barn);
  long countByFavoriteColor(Color color);
  long countByBarn(Barn barn);
}
