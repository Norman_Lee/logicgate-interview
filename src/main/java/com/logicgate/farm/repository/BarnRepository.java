package com.logicgate.farm.repository;

import com.logicgate.farm.domain.Barn;

import com.logicgate.farm.domain.Color;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarnRepository extends JpaRepository<Barn, Long> {

  // additional methods can be defined here
  List<Barn> findByColor(Color color);

  @Query("SELECT b FROM animal a JOIN a.barn b WHERE b.color =?1 GROUP BY b HAVING COUNT(a) < 20 ORDER BY COUNT(a) ASC")
  List<Barn> findHabitableBarnsOrderByCapacityAsc(Color color);

  @Query("SELECT b FROM animal a JOIN a.barn b WHERE b.color =?1 GROUP BY b HAVING COUNT(a) = ?2 ORDER BY COUNT(a) DESC")
  List<Barn> findByColorAndCapacityOrderByDesc(Color color, long capacity);

  long countByColor(Color color);
}
