package com.logicgate.farm.service;

import com.logicgate.farm.domain.Animal;
import com.logicgate.farm.domain.Barn;
import com.logicgate.farm.domain.Color;
import com.logicgate.farm.repository.AnimalRepository;
import com.logicgate.farm.repository.BarnRepository;

import com.logicgate.farm.util.FarmUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AnimalServiceImpl implements AnimalService {

  @Autowired
  private AnimalRepository animalRepository;

  @Autowired
  private BarnRepository barnRepository;

  @Override
  public Animal addToFarm(Animal animal) {

      List<Barn> barnsByColor = barnRepository.findByColor(animal.getFavoriteColor());

      List<Barn> habitableBarns = barnRepository
        .findHabitableBarnsOrderByCapacityAsc(animal.getFavoriteColor());

      //Checks if there are no barns of that color
      if(barnsByColor.isEmpty()){
        Barn barn = new Barn("", animal.getFavoriteColor());
        animal.setBarn(barn);
        barnRepository.save(barn);
      //Checks if there a habitable barns
      } else if(!habitableBarns.isEmpty()){
        Barn barn = habitableBarns.get(0);
        animal.setBarn(barn);
      } else {
      //Otherwise create a new barn and reallocate animals using a round robin up until I hit the average animalCount per barn
        Barn barn = new Barn ( "", animal.getFavoriteColor());
        barnRepository.save(barn);

        int animalCount = (FarmUtils.barnCapacity() * barnsByColor.size())/(barnsByColor.size() + 1);

        List<Animal> animalList = animalRepository
          .findByFavoriteColorOrderByBarnAsc(animal.getFavoriteColor());

        int barnCount = barnsByColor.size();
        int index = -1;
        for(int i = 0; i < animalCount; i++){
            int modIndex = i % barnCount;
            if(modIndex == 0){
              index++;
            }
            Animal a = animalList.get(modIndex * FarmUtils.barnCapacity() + index);
            a.setBarn(barn);
            animalRepository.save(a);
        }

        animal.setBarn(barn);
      }

      animalRepository.save(animal);
      return animal;
  }

  @Override
  public void removeFromFarm(Animal animal) {

    //Removal will happen anyways regardless, doing this first to get an accurate after state of the barns
    animalRepository.delete(animal.getId());

    Color animalColor = animal.getFavoriteColor();
    long totalAnimalCountByColor = animalRepository.countByFavoriteColor(animalColor);
    long totalBarnCountByColor =barnRepository.countByColor(animalColor);
    Barn barn = animal.getBarn();
    //Retrieving animals that need to potentially need to be reallocated
    List<Animal> animals = animalRepository.findByBarn(barn);

    //Checks if the animals can fit in a smaller set of barns
    if(totalAnimalCountByColor <= (totalBarnCountByColor - 1) * FarmUtils.barnCapacity()){
      List<Barn> barns = barnRepository.findHabitableBarnsOrderByCapacityAsc(animalColor);

      //Removes the barn to be removed from the list
      if(barns.contains(barn)){
          barns.remove(barn);
      }

      //Sets up a round robin to redistribute, although I think all I really need to do here is cap out every barn up till 20
      int barnCount = barns.size();
      List<Long> animalCounts = new ArrayList<>();
      barns.forEach(b -> animalCounts.add(animalRepository.countByBarn(b)));

      int index = 0;
      int barnIndex = 0;
      while(index < animals.size()){
        long animalCount = animalCounts.get(barnIndex);
        if(animalCount < FarmUtils.barnCapacity()){
          Barn b = barns.get(barnIndex);
          Animal a = animals.get(index);
          a.setBarn(b);
          animalRepository.save(a);
          animalCounts.set(barnIndex,animalCount + 1);
          index++;

        }
        barnIndex = (barnIndex +1) % barnCount;
      }
      barnRepository.delete(barn.getId());
      //Readjusts weights of the barns so every barn has no less than the floor of the average number of animals per barn if needed
    } else if(animals.size() < (totalAnimalCountByColor / totalBarnCountByColor)){
        List<Barn> barns = barnRepository.findByColorAndCapacityOrderByDesc(animalColor, animals.size() +2);
        List<Animal> as = animalRepository.findByBarn(barns.get(0));
        Animal a = as.get(0);
        a.setBarn(barn);
        animalRepository.save(a);
    }
  }

  @Override
  public void addToFarm(List<Animal> animals) {
    animals.forEach(this::addToFarm);
  }

  @Override
  public void removeFromFarm(List<Animal> animals) {
    animals.forEach(animal -> removeFromFarm(animalRepository.findOne(animal.getId())));
  }

  @Override
  public List<Animal> findAll() {
    return animalRepository.findAll();
  }

  @Override
  public void deleteAll() {
    animalRepository.deleteAll();
  }

}
